# rqt_valkyrie_dashboard
## Overivew
RQT tool to monitor the status of NASA's Valkyrie

![rqt_valkyrie_dashboard](https://bitbucket.org/spaceroboticschallengeme/rqt_valkyrie_dashboard/wiki/images/display_eg.png)

## Subscribing topics
* /ihmc_ros/valkyrie/output/behavior
* /ihmc_ros/valkyrie/output/footstep_status
* /ihmc_ros/valkyrie/output/high_level_state_change
* /ihmc_ros/valkyrie/output/robot_motion_status
* /ihmc_ros/valkyrie/output/walking_status

## Install
```bash
$ sudo apt-get install python-notify2
$ cd catkin_ws/src [to Your catkin workspace]
$ git clone https://bitbucket.org/spaceroboticschallengeme/rqt_valkyrie_dashboard.git
$ catkin_make
```

## Usage

```bash
$ rqt
```

Plugins -> Robot -> Valkyrie -> Valkyrie Dashboard


OR


```bash
$ roslaunch rqt_valkyrie_dashboard dashboard.launch
```