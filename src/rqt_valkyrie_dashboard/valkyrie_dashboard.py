#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import rospkg
import roslib
roslib.load_manifest('rqt_valkyrie_dashboard')
import rospy
import tf2_ros
from tf import transformations

import std_srvs.srv
from std_msgs.msg import String, Int32, Empty, Bool
from nav_msgs.msg import Odometry

from rqt_robot_dashboard.dashboard import Dashboard
from rqt_robot_dashboard.monitor_dash_widget import MonitorDashWidget
from rqt_robot_dashboard.console_dash_widget import ConsoleDashWidget

from python_qt_binding.QtCore import QSize
from python_qt_binding.QtGui import QMessageBox
from python_qt_binding import loadUi

import ihmc_msgs.msg
from srcsim.msg import Satellite, Task

from python_qt_binding.QtWidgets import QLabel, QWidget

#from PyQt4 import QtGui, QtCore

# Ref: https://bitbucket.org/osrf/srcsim/wiki/api
# Ref: https://github.com/ihmcrobotics/ihmc-open-robotics-software/blob/develop/SensorProcessing/src/us/ihmc/sensorProcessing/model/RobotMotionStatus.java
# Ref: https://github.com/AriYu/rqt_mypkg

class ValkyrieDashboard(Dashboard):
    """
    Dashboard for NASA's Valkyrie
    :param context: the plugin context
    :type context: qt_gui.plugin.Plugin
    """

    def setup(self, context):
        self.launched = False
        self.name = 'Valkyrie Dashboard'
        self.max_icon_size = QSize(50, 30)
        self.message = None

        self._widget = QWidget()
        self._widget.setObjectName('ValkyrieUI')
        ui_file = os.path.join(rospkg.RosPack().get_path('rqt_valkyrie_dashboard'), 'resource', 'Dashboard.ui')
        loadUi(ui_file, self._widget)
        #context.add_widget(self._widget)
        self._widget.ledButton.clicked[bool].connect(self._handle_led_clicked)
        self._widget.harnessButton.clicked[bool].connect(self._handle_harness_clicked)

        self._dashboard_message = None
        self._last_dashboard_message_time = 0.0

        self._raw_byte = None
        self.digital_outs = [0, 0, 0]

        # ROS Console, Monitor button
        #self._console = ConsoleDashWidget(self.context, minimal=False)
        #self._monitor = MonitorDashWidget(self.context)

        #self._widget = QWidget()
        self._high_level_state_label = QLabel("  RoboState: ")
        self._high_level_state_text  = QLabel("N/A        ")

        self._robot_motion_status_label = QLabel("  Motion: ")
        self._robot_motion_status_text = QLabel("N/A      ")

        self._behavior_label = QLabel("  Behavior: ")
        self._behavior_text = QLabel("N/A      ")

        self._walking_status_label = QLabel("  Walking: ")
        self._walking_motion_status_text = QLabel("N/A      ")

        self._footstep_status_label = QLabel("  Footstep: ")
        self._footstep_status_text = QLabel("N/A")

        # Subscribers
        self._footstep_status_sub = rospy.Subscriber('/ihmc_ros/valkyrie/output/footstep_status', ihmc_msgs.msg.FootstepStatusRosMessage, self.footstep_status_callback)
        #self._high_level_state_sub = rospy.Subscriber('/ihmc_ros/valkyrie/output/high_level_state', ihmc_msgs.msg.HighLevelStateRosMessage, self.high_level_state_callback)
        self._high_level_state_sub = rospy.Subscriber('/ihmc_ros/valkyrie/output/high_level_state_change', ihmc_msgs.msg.HighLevelStateChangeStatusRosMessage, self.high_level_state_callback)
        self._robot_motion_status_sub = rospy.Subscriber('/ihmc_ros/valkyrie/output/robot_motion_status', String, self.robot_motion_status_callback)
        self._walking_status_sub = rospy.Subscriber('/ihmc_ros/valkyrie/output/walking_status', ihmc_msgs.msg.WalkingStatusRosMessage, self.walking_status_callback)
        self._behavior_sub = rospy.Subscriber('/ihmc_ros/valkyrie/output/behavior', Int32, self.behavior_callback)
        self._pose_sub = rospy.Subscriber('/ihmc_ros/valkyrie/output/robot_pose', Odometry, self.pose_callback)
        self._hand_traj_sub = rospy.Subscriber('/ihmc_ros/valkyrie/control/hand_trajectory', ihmc_msgs.msg.HandTrajectoryRosMessage, self.hand_traj_callback)
        self._src_dish_sub = rospy.Subscriber('/task1/checkpoint2/satellite', Satellite, self.src_dish_callback)
        self._src_task_sub = rospy.Subscriber('/ocu/srcsim/finals/task', Task, self.src_task_callback)

        self._led_pub = rospy.Publisher('/srcsim/qual1/start', Empty, queue_size=1) # Start Qualitification Task 1
        self._harness_pub = rospy.Publisher('/valkyrie/harness/detach', Bool, queue_size=1) # Release Valkyrie's harness for start

        rospy.Timer(rospy.Duration(0.1), self.timer_callback)

        self.tfBuffer = tf2_ros.Buffer()
        self.tf_listener = tf2_ros.TransformListener(self.tfBuffer)

        self._widget_4 = QWidget()
        self._widget_4.setObjectName('ValkyrieUI_4nd')
        ui_file = os.path.join(rospkg.RosPack().get_path('rqt_valkyrie_dashboard'), 'resource', '4nd_dan.ui')
        loadUi(ui_file, self._widget_4)
        context.add_widget(self._widget_4)

        self._widget_2 = QWidget()
        self._widget_2.setObjectName('ValkyrieUI_2nd')
        ui_file = os.path.join(rospkg.RosPack().get_path('rqt_valkyrie_dashboard'), 'resource', '2nd_dan.ui')
        loadUi(ui_file, self._widget_2)
        context.add_widget(self._widget_2)

        self._widget_3 = QWidget()
        self._widget_3.setObjectName('ValkyrieUI_3nd')
        ui_file = os.path.join(rospkg.RosPack().get_path('rqt_valkyrie_dashboard'), 'resource', '3nd_dan.ui')
        loadUi(ui_file, self._widget_3)
        context.add_widget(self._widget_3)

        

        self.launched = True

    def get_widgets(self):
        return [
                #[self._monitor, self._console], 
                [self._high_level_state_label, self._high_level_state_text], 
                [self._robot_motion_status_label, self._robot_motion_status_text], [self._behavior_label, self._behavior_text], 
                [self._walking_status_label, self._walking_motion_status_text], [self._footstep_status_label, self._footstep_status_text],
                #[self._widget]
        ]


    def src_dish_callback(self, msg):
        if self.launched == True:
            self._widget_3.text_target_pitch.setText(str('%02.2f' % (msg.target_pitch/3.14*180.0)) + "[deg]")
            self._widget_3.text_current_pitch.setText("<--    " + str('%02.2f' % (msg.current_pitch/3.14*180.0)) + "[deg]")
            self._widget_3.text_target_yaw.setText(str('%02.2f' % (msg.target_yaw/3.14*180.0)) + "[deg]")
            self._widget_3.text_current_yaw.setText("<--    " + str('%02.2f' % (msg.current_yaw/3.14*180.0)) + "[deg]")

            if msg.pitch_correct_now == True:
                self._widget_3.text_status_pitch.setText("OK")
                if msg.pitch_completed == True:
                    self._widget_3.text_status_pitch.setText("[OK]")
            else:
                self._widget_3.text_status_pitch.setText("NG")

            if msg.yaw_correct_now == True:
                self._widget_3.text_status_yaw.setText("OK")
                if msg.yaw_completed == True:
                    self._widget_3.text_status_yaw.setText("[OK]")
            else:
                self._widget_3.text_status_yaw.setText("NG")

    def src_task_callback(self, msg):
        if self.launched == True:

            if msg.task == 1:
                if msg.current_checkpoint == 1:
                    self._widget_4.text_task.setText("T1C1: Move within 1 meter from the communication dish")
                elif msg.current_checkpoint == 2:
                    self._widget_4.text_task.setText("T1C2: Move the communication dish either to the correct pitch or the correct yaw angle")
                elif msg.current_checkpoint == 3:
                    self._widget_4.text_task.setText("T1C3: Move the communication dish to both correct pitch and yaw angles")
                elif msg.current_checkpoint == 4:
                    self._widget_4.text_task.setText("T1C4: Walk into Task 1's finish box")
            elif msg.task == 2:
                if msg.current_checkpoint == 1:
                    self._widget_4.text_task.setText("T2C1: Retrieve the solar panel from the rover")
                elif msg.current_checkpoint == 2:
                    self._widget_4.text_task.setText("T2C2: Place the solar panel close to the power cable")
                elif msg.current_checkpoint == 3:
                    self._widget_4.text_task.setText("T2C3: Press the button on the solar panel")
                elif msg.current_checkpoint == 4:
                    self._widget_4.text_task.setText("T2C4: Pick up the power cable")
                elif msg.current_checkpoint == 5:
                    self._widget_4.text_task.setText("T2C5: Plug the power cable into the solar panel")
                elif msg.current_checkpoint == 6:
                    self._widget_4.text_task.setText("T2C6: Walk into Task 2's finish box")
            elif msg.task == 3:
                if msg.current_checkpoint == 1:
                    self._widget_4.text_task.setText("T3C1: Climb the stairs")
                elif msg.current_checkpoint == 2:
                    self._widget_4.text_task.setText("T3C2: Open the door")
                elif msg.current_checkpoint == 3:
                    self._widget_4.text_task.setText("T3C3: Pass through the door")
                elif msg.current_checkpoint == 4:
                    self._widget_4.text_task.setText("T3C4: Pick up the leak detector")
                elif msg.current_checkpoint == 5:
                    self._widget_4.text_task.setText("T3C5: Find the leak")
                elif msg.current_checkpoint == 6:
                    self._widget_4.text_task.setText("T3C6: Pick up the leak repair tool")
                elif msg.current_checkpoint == 7:
                    self._widget_4.text_task.setText("T3C7: Repair the leak")
                elif msg.current_checkpoint == 8:
                    self._widget_4.text_task.setText("T3C8: Walk into Task 3's finish box")
            else:
                self._widget_4.text_task.setText("N/A")


    def timer_callback(self, event):
        #print 'Timer called at ' + str(event.current_real)
        if self.launched == True:
            try:
                rP_trans = self.tfBuffer.lookup_transform('world', 'rightPalm', rospy.Time(0))
                rP_pos = rP_trans.transform.translation
                self._widget_2.text_r_hand.setText("[" + str('%02.2f' % rP_pos.x) + ", " + str('%02.2f' % rP_pos.y) + ", " + str('%02.2f' % rP_pos.z) + "]@world" )
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
                pass

    def hand_traj_callback(self, msg):
        pos = msg.taskspace_trajectory_points[-1].position
        quat = msg.taskspace_trajectory_points[-1].orientation
        (roll, pitch, yaw) = transformations.euler_from_quaternion([quat.x, quat.y, quat.z, quat.w])
        self._widget_2.text_r_hand_cmd.setText("[" + str('%02.2f' % pos.x) + ", " + str('%02.2f' % pos.y) + ", " + str('%02.2f' % pos.z) +
                                               "]@world RPY(" + 
                                               str('%03.1f' % (roll/3.14*180.0)) + ", " + str('%03.1f' % (pitch/3.14*180.0)) + ", " + str('%03.1f' % (yaw/3.14*180.0)) + 
                                               ")"
        )

    def pose_callback(self, msg):
        if self.launched == True:
            (roll, pitch, yaw) = transformations.euler_from_quaternion((msg.pose.pose.orientation.x, msg.pose.pose.orientation.y, msg.pose.pose.orientation.z, msg.pose.pose.orientation.w))
            self._widget_2.text_pose.setText("(" + str('%02.2f' % msg.pose.pose.position.x) + ", " + str('%02.2f' % msg.pose.pose.position.y) + ", " + str('%02.2f' % msg.pose.pose.position.z) + ")[m] , (" + str('%02.2f' % (roll/3.14*180.0)) + "," + str('%02.2f' % (pitch/3.14*180.0)) + "," + str('%02.2f' % (yaw/3.14*180.0)) +  ")[deg]")

    def behavior_callback(self, msg):
        state = msg.data

        if state == 3:
            self._behavior_text.setText("STANDING ")
        elif state == 4:
            self._behavior_text.setText("IN_MOTION")
        else:
            self._behavior_text.setText("UNKNOWN  ")

    def footstep_status_callback(self, msg):
        state = msg

        str1 = ""
        if state.status == 0:
            str1 = "STARTED"
        elif state.status == 1:
            str1 = "COMPLETED"
        else:
            str1 = "UNKNOWN"

        str2 = str(state.footstep_index)
        
        str3 = ""
        if state.robot_side == 0:
            str3 = "LEFT"
        elif state.robot_side == 1:
            str3 = "RIGHT"

        self._footstep_status_text.setText(str1 + ": step(" + str2 + ")@" + str3)


    def high_level_state_callback(self, msg):
        state = msg.end_state

        if state == 0:
            self._high_level_state_text.setText("WALKING    ")
        elif state == 1:
            self._high_level_state_text.setText("DO_NOTHING ")
        elif state == 2:
            self._high_level_state_text.setText("DIAGNOSTICS")
        elif state == 3:
            self._high_level_state_text.setText("CALIBRATION")
        else:
            self._high_level_state_text.setText("Unknown    ")

    def robot_motion_status_callback(self, msg):
        self._robot_motion_status_text.setText(msg.data)

    def walking_status_callback(self, msg):
        state = msg.status

        if state == 0:
            self._walking_motion_status_text.setText("STARTED  ")
        elif state == 1:
            self._walking_motion_status_text.setText("COMPLETED")
        elif state == 2:
            self._walking_motion_status_text.setText("ABORT_REQ")
        else:
            self._walking_motion_status_text.setText("Unknown  ")

    def shutdown_dashboard(self):
        self.launched = False
        self._footstep_status_sub.unregister()
        self._high_level_state_sub.unregister()
        self._robot_motion_status_sub.unregister()
        self._walking_status_sub.unregister()
        self._behavior_sub.unregister()

    def save_settings(self, plugin_settings, instance_settings):
        #self._console.save_settings(plugin_settings, instance_settings)
        #self._monitor.save_settings(plugin_settings, instance_settings)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        #self._console.restore_settings(plugin_settings, instance_settings)
        #self._monitor.restore_settings(plugin_settings, instance_settings)
        pass
        
    def _handle_led_clicked(self):
        msg = Empty()
        self._led_pub.publish(msg)

    def _handle_harness_clicked(self):
        msg = Bool()
        msg.data = True
        self._harness_pub.publish(msg)
        
